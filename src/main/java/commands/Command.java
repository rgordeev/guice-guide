package commands;

import com.google.inject.Inject;
import model.Model;
import services.ForTest;
import services.StorageService;

/**
 * (c) Roman Gordeev
 * <p/>
 * 2014 июн 24
 */
public class Command
{
    public Command(Model model)
    {
        this.model = model;
    }

    public void execute()
    {
        storage.store(model);
    }

    @Inject
    public void setStorage(StorageService storage) {
        this.storage = storage;
    }

    private StorageService storage;
    private Model          model;
}
