package commands;

import model.Model;
import services.StorageServiceFactory;

/**
 * (c) Roman Gordeev
 * <p/>
 * 2014 июн 25
 */
public class CommandFactory
{
    private CommandFactory() {}

    public static Command createCommand(Model model)
    {
        return new Command(model);//return new Command(model, StorageServiceFactory.getInstance().createStorage());
    }
}
