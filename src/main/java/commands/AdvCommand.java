package commands;

import com.google.inject.Inject;
import model.Model;
import services.Advanced;
import services.StorageService;

/**
 * (c) Roman Gordeev
 * <p/>
 * 2014 июн 25
 */
public class AdvCommand
{
    public AdvCommand(Model model) {
        this.model = model;
    }

    public void execute()
    {
        storage.store(model);
    }

    @Inject
    public void setStorage(@Advanced StorageService storage) {
        this.storage = storage;
    }

    private Model model;
    private StorageService storage;
}
