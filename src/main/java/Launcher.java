import com.google.inject.Guice;
import com.google.inject.Injector;
import commands.AdvCommand;
import commands.Command;
import commands.CommandFactory;
import model.Model;
import services.StorageModule;
import services.StorageServiceFactory;

/**
 * (c) Roman Gordeev
 * <p/>
 * 2014 июн 24
 */
public class Launcher
{

    public static void main(String[] args)
    {
        Model model = new Model("model1");

        //Command c = CommandFactory.createCommand(model);
        Injector injector = Guice.createInjector(new StorageModule());

        Command c = new Command(model);
        injector.injectMembers(c);

        c.execute();

        AdvCommand c1 = new AdvCommand(model);
        injector.injectMembers(c1);

        c1.execute();
    }
}
