package model;

/**
 * (c) Roman Gordeev
 * <p/>
 * 2014 июн 24
 */
public class Model
{
    private String title;

    public Model(String title)
    {
        this.title = title;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }
}
