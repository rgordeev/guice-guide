package services;

import model.Model;

/**
 * (c) Roman Gordeev
 * <p/>
 * 2014 июн 24
 */
public interface StorageService
{
    void store(Model model);
}
