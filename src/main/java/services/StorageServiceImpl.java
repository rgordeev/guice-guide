package services;

import com.google.inject.Singleton;
import model.Model;

/**
 * (c) Roman Gordeev
 * <p/>
 * 2014 июн 24
 */
@Singleton
public class StorageServiceImpl implements StorageService
{
    @Override
    public void store(Model model)
    {
        System.out.println(model.getTitle());
    }
}
