package services;

import com.google.inject.AbstractModule;

/**
 * (c) Roman Gordeev
 * <p/>
 * 2014 июн 25
 */
public class StorageModule extends AbstractModule
{
    @Override
    protected void configure()
    {
        bind(StorageService.class).to(StorageServiceImpl.class);
        bind(StorageService.class).annotatedWith(Advanced.class).to(AdvancedStorageService.class);

    }
}
