package services;

import model.Model;

/**
 * (c) Roman Gordeev
 * <p/>
 * 2014 июн 25
 */
public class AdvancedStorageService implements StorageService
{
    @Override
    public void store(Model model)
    {
        System.out.println("Advansed: " + model.getTitle());
    }
}
