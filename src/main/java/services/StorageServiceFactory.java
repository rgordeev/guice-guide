package services;

/**
 * (c) Roman Gordeev
 * <p/>
 * 2014 июн 25
 */
public class StorageServiceFactory
{
    private static final StorageServiceFactory instance = new StorageServiceFactory();

    public static StorageServiceFactory getInstance()
    {
        return instance;
    }

    public StorageServiceFactory()
    {
        this.storage = new AdvancedStorageService();
    }

    public StorageService createStorage()
    {
        return storage;
    }

    private StorageService storage;
}
