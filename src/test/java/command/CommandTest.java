package command;

import com.google.inject.Guice;
import com.google.inject.Injector;
import commands.Command;
import commands.CommandFactory;
import model.Model;
import modules.TestModule;
import org.junit.Test;
import services.ForTest;
import services.StorageService;

import static org.junit.Assert.assertTrue;

/**
 * (c) Roman Gordeev
 * <p/>
 * 2014 июн 25
 */
public class CommandTest
{
    @Test
    public void storeTest()
    {
        StorageServiceMock serviceMock = new StorageServiceMock();

        Model m = new Model("test model");
        Command c = CommandFactory.createCommand(m);
        c.setStorage(serviceMock);
        c.execute();

        assertTrue(serviceMock.isCalled());
    }

    public static class StorageServiceMock implements StorageService
    {
        @Override
        public void store(Model model)
        {
            called = true;
            System.out.println(model.getTitle());
        }

        public boolean isCalled()
        {
            return called;
        }

        private boolean called = false;
    }
}
