package modules;

import com.google.inject.AbstractModule;
import command.CommandTest;
import services.ForTest;
import services.StorageService;

/**
 * (c) Roman Gordeev
 * <p/>
 * 2014 июн 25
 */
public class TestModule extends AbstractModule
{
    @Override
    protected void configure() {
        bind(StorageService.class).annotatedWith(ForTest.class).to(CommandTest.StorageServiceMock.class);
    }
}
